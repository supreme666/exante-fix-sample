# Demonstrates use of basic FIX functionality provided by EXANTE Prime broker (www.exante.eu)
* Getting Real-time market data
* Getting available instruments list from EXANTE
* Placing Limit and Market orders. Cancel Orders
* Getting order status updates
* Mass order status request feature
* Getting account summary update


# How to use 

## Get FIX credentials from EXANTE's support team ##
- SenderCompID=YOUR_TRADE_UAT
- password=YOURPASSWORD
- other credential reveived from EXANTE if different


## Update configuration ##
Place yours credentials into two files "SimpleFIXDemo\Configs\fix_vendor.ini.template" and "SimpleFIXDemo\Configs\fix_broker.ini.template"

Rename files to  "SimpleFIXDemo\Configs\fix_vendor.ini" and "SimpleFIXDemo\Configs\fix_broker.ini" accordingly

## Compile Solution ##

## Enjoy ##

Should you have any questions on EXANTE FIX integration do not hesitate to contact support@exante.eu