﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Globalization;
using System.Text;
using ExanteFIXWrapper;
using System.Collections.Concurrent;
using System.Windows.Forms;

namespace SimpleFIXDemo
{
    partial class MainForm
    {
        private String requestIdMD;
        private SymbolBase symbol;

        /// <summary>
        /// FIX part
        /// </summary>
        /// <param name="symbol"></param>
        private void SubscribeForMarketData(SymbolBase symbol)
        {
            try
            {
                this.requestIdMD = System.Guid.NewGuid().ToString();
                feedFIX.SubscribeForQuotes(requestIdMD, symbol, updateQuotes);
            }
            catch (Exception)
            {
                // not connected?
            }
        }
        
        public void updateQuotes(String requestId, Dictionary<Double, Double> bids, Dictionary<Double, Double> asks, Dictionary<Double, Double> trades, DateTime timestamp)
        {
            if (!this.requestIdMD.Equals(requestId))
                return;
            if (bids.Keys.Count > 0 && asks.Keys.Count > 0)
            {
                listViewMarketDepth.Invoke(new Action(() =>
                {
                    SuspendConrolRedraw(listViewMarketDepth);
                    listViewMarketDepth.BeginUpdate();
                    listViewMarketDepth.Items.Clear();
                    foreach (Double price in bids.Keys)
                    {
                        ListViewItem bid = new ListViewItem(new string[] { price.ToString(), bids[price].ToString() });
                        bid.BackColor = Color.LightBlue;
                        listViewMarketDepth.Items.Add(bid);
                    }
                    foreach (Double price in asks.Keys)
                    {
                        ListViewItem ask = new ListViewItem(new string[] { price.ToString(), asks[price].ToString() });
                        ask.BackColor = Color.LightYellow;
                        listViewMarketDepth.Items.Add(ask);
                    }
                    listViewMarketDepth.Sort();
                    listViewMarketDepth.EndUpdate();
                    ResumeControlRedraw(listViewMarketDepth);
                }
                ));
            }
            if (trades.Keys.Count > 0)
            {
                labelLastTradeValue.Invoke(new Action(() =>
                    {
                        Double price = trades.Keys.First();
                        labelLastTradeValue.Text = trades[price].ToString() + "@" + price.ToString() + " (" + timestamp.ToShortTimeString() + ")";
                    }
                ));
            }
        }

        private void PlaceOrder(Position position, OrderType orderType)
        {
            labelPlaceOrderError.Visible = false;
            try
            {
                switch (orderType)
                {
                    case OrderType.MARKET:
                        executionFIX.placeOrder(new OrderMarket(DateTime.Now, symbol, position, ReadQuantity()), OrderStatusUpdate);
                        break;
                    case OrderType.LIMIT:
                        executionFIX.placeOrder(new OrderLimit(DateTime.Now, symbol, position, ReadQuantity(), ReadPrice()), OrderStatusUpdate);
                        break;
                }
            }
            catch (Exception ex)
            {
                labelPlaceOrderError.Text = ex.Message;
                labelPlaceOrderError.Visible = true;
            }
        }


        /// <summary>
        /// UI Part
        /// </summary>
        private void ClearDispalayComponents()
        {
            if (this.IsHandleCreated)
            {
                listViewMarketDepth.Invoke(new Action(() => listViewMarketDepth.Items.Clear()));
                labelLastTradeValue.Invoke(new Action(() => labelLastTradeValue.Text = ""));
            }
        }        

        private void InitMarketDepthControl()
        {
            SetDoubleBuffered(listViewMarketDepth);
            listViewMarketDepth.Columns.Add("Price");
            listViewMarketDepth.Columns.Add("Volume");
            listViewMarketDepth.Sorting = SortOrder.Descending;
        }

        private void SetActiveSymbol(SymbolBase symbol)
        {
            // TODO: unsubscribe from old symbol
            ClearDispalayComponents();
            this.symbol = symbol;
            labelActiveSymbol.Text = this.symbol.ToString();
            SubscribeForMarketData(symbol);
        }

        private Int32 ReadQuantity()
        {
            Int32 quantity = 0;
            if (Int32.TryParse(textBoxQuantity.Text.Trim(), System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out quantity) && quantity > 0) //CultureInfo.InvariantCulture);
            {
                return quantity;
            }
            throw new Exception("Wrong quantity format");
        }

        private Double ReadPrice()
        {
            Double price = 0;
            if (double.TryParse(textBoxPrice.Text.Trim(), System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out price) && price > 0) //CultureInfo.InvariantCulture);
            {
                return price;
            }
            throw new Exception("Wrong price format");
        }

        private void buttonBuyMarket_Click(object sender, EventArgs e)
        {
            PlaceOrder(Position.Long, OrderType.MARKET);
        }

        private void buttonSellMarket_Click(object sender, EventArgs e)
        {
            PlaceOrder(Position.Short, OrderType.MARKET);
        }

        private void buttonBuyLimit_Click(object sender, EventArgs e)
        {
            PlaceOrder(Position.Long, OrderType.LIMIT);
        }

        private void buttonSellLimit_Click(object sender, EventArgs e)
        {
            PlaceOrder(Position.Short, OrderType.LIMIT);
        }
    }
}
