﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExanteFIXWrapper;
using System.Collections.Concurrent;

namespace SimpleFIXDemo
{
    partial class MainForm
    {
        private String requestIDSymbols;
        private int totalSymbolsCount = 0;
        private ConcurrentBag<SymbolBase> allSymbols = new ConcurrentBag<SymbolBase>();

        /// <summary>
        /// FIX part
        /// </summary>
        public void UpdateSymbolsList(String requestId, List<SymbolBase> symbols, Int32 totalSymbolsCount)
        {
            if (this.requestIDSymbols != requestId)
                return;
            this.totalSymbolsCount = totalSymbolsCount;
            foreach (var elem in symbols)
            {
                allSymbols.Add(elem);
            }
            UpdateProgressBar();
        }

        private void GetSymbolsList()
        {
            this.requestIDSymbols = System.Guid.NewGuid().ToString();
            executionFIX.GetSymbolsList(requestIDSymbols, UpdateSymbolsList);
        }


        /// <summary>
        /// UI Part
        /// </summary>
        private void UpdateShownSymbolCount()
        {
            labelShownCount.Invoke(new Action(() =>
            {
                labelShownCount.Text = listBoxSymbols.Items.Count.ToString() + @" / " + this.totalSymbolsCount.ToString();
            }
            ));
        }

        private void UpdateProgressBar()
        {
            progressBarLoadSymbols.Invoke(new Action(() =>
            {
                progressBarLoadSymbols.Maximum = this.totalSymbolsCount;
                progressBarLoadSymbols.Value = allSymbols.Count;
                if (allSymbols.Count >= totalSymbolsCount)
                {
                    progressBarLoadSymbols.Visible = false;
                    listBoxSymbols.Enabled = true;
                    UpdateListBoxSymbols();
                }
            }
            ));
        }


        private void UpdateListBoxSymbols()
        {
            listBoxSymbols.Invoke(new Action(() =>
            {
                SuspendConrolRedraw(listBoxSymbols);
                listBoxSymbols.Items.Clear();
                foreach (var symbol in allSymbols.ToArray())
                {
                    if (symbol.EXANTEId.IndexOf(textBoxFilter.Text.Trim(), 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                        listBoxSymbols.Items.Add(symbol);
                }
                ResumeControlRedraw(listBoxSymbols);
                UpdateShownSymbolCount();
            }
            ));
        }


        private void buttonRequestSymbols_Click(object sender, EventArgs e)
        {
            listBoxSymbols.Enabled = false;
            progressBarLoadSymbols.Visible = true;
            progressBarLoadSymbols.Value = 0;
            buttonRequestSymbols.Enabled = false;
            GetSymbolsList();
        }

        private void textBoxFilter_TextChanged(object sender, EventArgs e)
        {
            UpdateListBoxSymbols();
        }

        private void listBoxSymbols_DoubleClick(object sender, EventArgs e)
        {
            SetActiveSymbol((SymbolBase)listBoxSymbols.SelectedItem);
        }
    }
}
