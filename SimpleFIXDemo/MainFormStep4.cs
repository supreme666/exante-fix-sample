﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading;
using System.Globalization;
using ExanteFIXWrapper;
using System.Windows.Forms;
using System.Collections.Concurrent;


namespace SimpleFIXDemo
{
    partial class MainForm
    {
        private Thread updateASThread;
        private String requestIdAS = null;
        bool isResponseCompleted = true;
        Int16 numberOfResponse;

        /// <summary>
        /// FIX part
        /// </summary>
        private void UpdateAccountSummary()
        {
            try
            {

                this.requestIdAS = System.Guid.NewGuid().ToString();
                executionFIX.AccountSummaryRequest(requestIdAS, AccountSummaryCallback);
            }
            catch (Exception)
            {
                // not connected?
            }
        }

        private void AccountSummaryCallback(String requestId, String ExanteId, Position position, Double quantity, Int32 totalNumberOfSymbols, DateTime timestamp)
        {
            if (!this.requestIdAS.Equals(requestIdAS))
                return;
            if (this.IsHandleCreated)
            {
                listViewAccountSummary.Invoke(new Action(() =>
                {
                    if (isResponseCompleted == true) // Previous response was completed. New response started
                    {
                        SuspendConrolRedraw(listViewAccountSummary);
                        isResponseCompleted = false;
                        numberOfResponse = 1;
                        listViewAccountSummary.Items.Clear();
                    }
                    ListViewItem row = new ListViewItem(new string[] { ExanteId, position.ToString(), quantity.ToString() });
                    listViewAccountSummary.Items.Add(row);
                    listViewAccountSummary.Sort();

                    if (totalNumberOfSymbols == numberOfResponse++)
                    {
                        isResponseCompleted = true;
                        ResumeControlRedraw(listViewAccountSummary);
                    }
                }
                ));
            }
        }



        /// <summary>
        /// UI part
        /// </summary>
        private void InitAccountSummaryControl()
        {
            SetDoubleBuffered(listViewAccountSummary);
            listViewAccountSummary.Columns.Add(@"ExanteId/Currency", 150);
            listViewAccountSummary.Columns.Add("Position");
            listViewAccountSummary.Columns.Add("Value");
            listViewAccountSummary.Sorting = SortOrder.Descending;
            updateASThread = new Thread(UpdateASThread);
            updateASThread.Start();
        }

        private void UpdateASThread()
        {
            while (true)
            {
                System.Threading.Thread.Sleep(1000);
                if (checkBoxAutoUpdateAS.Checked)
                    UpdateAccountSummary();
            }
        }

        private void buttonReloadAS_Click(object sender, EventArgs e)
        {
            UpdateAccountSummary();
        }
    }
}
