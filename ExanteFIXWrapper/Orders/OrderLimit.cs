﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExanteFIXWrapper
{
    public class OrderLimit: OrderBase
    {
        private Double price;

        public OrderLimit(DateTime dateTime, SymbolBase symbol, Position position, Int32 quantity, Double price)
            : base(dateTime, symbol, position, quantity)
        {
            this.price = price;
        }

        public OrderLimit(String clientOrderId, DateTime dateTime, SymbolBase symbol, Position position, Int32 quantity, Double price)
            : base(clientOrderId, dateTime, symbol, position, quantity)
        {
            this.price = price;
        }


        public Double Price
        {
            get { return price; }
        }
    }
}
