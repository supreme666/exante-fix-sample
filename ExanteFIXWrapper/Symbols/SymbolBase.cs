﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExanteFIXWrapper
{
    public enum ExecutionType {MARKET, LIMIT};
    public class SymbolBase :ICloneable
    {
        // Common params
        private String exanteId;
        private String name;

        public String EXANTEId
        {
            get { return exanteId; }
            set { exanteId = value; }
        }

        public String Name
        {
            get { return name; }
            set { name = value; }
        }
        private TimeSpan sessionStart;
        public TimeSpan SessionStart
        {
            get { return sessionStart; }
            set { this.sessionStart = value; }
        }

        private TimeSpan sessionEnd;
        public TimeSpan SessionEnd
        {
            get { return sessionEnd; }
            set { this.sessionEnd = value; }
        }

        public override String ToString()
        {
            return exanteId;
        }
        
        // Common Data feed params
        private String cfiCode;
        public String CFICode
        {
            get { return cfiCode; }
            set { this.cfiCode = value; }
        }

        // Common Execution params
        ExecutionType executionType;
        public SymbolBase(String exanteId)
        {
            this.exanteId = exanteId;
        }

        
        public SymbolBase(String exanteId, TimeSpan sessionStart, TimeSpan sessionEnd, ExecutionType executionType)
        {
            this.exanteId = exanteId;
            this.sessionStart = sessionStart;
            this.sessionEnd = sessionEnd;
            this.executionType = executionType;
        }

        public SymbolBase(String exanteId, String name, String CFICode, TimeSpan sessionStart, TimeSpan sessionEnd, ExecutionType executionType)
        {
            this.exanteId = exanteId;
            this.name = name;
            this.CFICode = CFICode;
            this.sessionStart = sessionStart;
            this.sessionEnd = sessionEnd;
            this.executionType = executionType;
        }

        public object Clone()
        {
            return new SymbolBase(exanteId, name, CFICode, sessionStart, sessionEnd, executionType);
            throw new NotImplementedException();
        }
    }
}
