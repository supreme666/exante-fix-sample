﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using QuickFix;

namespace ExanteFIXWrapper
{
    public delegate void UpdateSymbolQuotesCallback(String requestId, Dictionary<Double, Double> bids, Dictionary<Double, Double> asks, Dictionary<Double, Double> trades, DateTime timestamp);

    public class QuoteFIXReceiver: IDisposable
    {
        private SocketInitiator initiator = null;
        private QuickFixFeedApp application = null;

        public void SubscribeForQuotes(String requestId, SymbolBase symbol, UpdateSymbolQuotesCallback updateQuotesCallback)
        {
            ((QuickFixFeedApp)application).AddSymbol(requestId, symbol, updateQuotesCallback);
        }

        ~QuoteFIXReceiver()
        {
            Dispose();
        }

        public QuoteFIXReceiver(String fixFeedIniPath)
        {
            try
            {
                SessionSettings settings = new SessionSettings(fixFeedIniPath);
                ArrayList sessions = settings.getSessions();
                QuickFix.Dictionary dict = settings.get((QuickFix.SessionID) sessions[0]);
                String fixFeedPassword = dict.getString("password");
                FileStoreFactory storeFactory = new FileStoreFactory(settings);
                application = new QuickFixFeedApp(fixFeedPassword);
                FileLogFactory logFactory = new FileLogFactory(settings);
                MessageFactory messageFactory = new DefaultMessageFactory();
                initiator = new SocketInitiator(application, storeFactory, settings, logFactory /*optional*/, messageFactory);
                initiator.start();
            }
            catch (ConfigError e)
            {
                Console.WriteLine(e);
            }
        }

        public void Dispose()
        {
            if (initiator != null)
            {
                application.Dispose();
                application = null;
                initiator = null;
                GC.Collect();
                // Write solution hangs - known issue
                //initiator.stop(true);
                //initiator.Dispose();
                //initiator = null;
            }
        }

        public bool IsConnected()
        {
            if (initiator == null)
                return false;
            return initiator.isLoggedOn();
        }
    }
}

